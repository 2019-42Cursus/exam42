#include "get_next_line.h"
#include <stdio.h>
#include <fcntl.h>

int	main(int argc,char **argv)
{
	char	*line;
	int	fd;

	fd = 0;
	line = "";
	if (argc == 2)
		fd = open(argv[1], O_RDONLY);
	while (line != NULL)
	{
		line = get_next_line(fd);
		printf("%s", line);
	}
	return (0);
}
