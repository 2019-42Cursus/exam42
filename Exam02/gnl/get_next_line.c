#include "get_next_line.h"

static int
	ft_strlen(char *str)
{
	int	i;

	i = 0;
	if (str)
		while (str[i])
			i++;
	return (i);
}

static void
	ft_strjoin(char **ret, char *buff)
{
	int		i;
	int		j;
	char	*str;

	i = ft_strlen(*ret);
	j = ft_strlen(buff);
	str = (char *)malloc(sizeof(char) * (i + j + 1));
	str[i + j] = '\0';
	i = 0;
	j = 0;
	if (*ret)
	{
		while ((*ret)[i])
		{
			str[i] = (*ret)[i];
			i++;
		}
		free(*ret);
	}
	if (buff)
	{
		while (buff[j])
		{
			str[i] = buff[j];
			i++;
			j++;
		}
	}
	*ret = str;
}

void	ft_zero(char *buff)
{
	int	i;

	i = 0;
	while (buff[i])
	{
		buff[i] = 0;
		i++;
	}
}

int	ft_return(char *str)
{
	int	i;

	i = 0;
	if (!str)
		return (1);
	while (str[i])
	{
		if (str[i] == '\n')
			return (0);
		i++;
	}
	return (1);
}

char	*get_next_line(int fd)
{
	static char	buff[BUFFER_SIZE + 1];
	char		*ret;
	int			i;
	int			j;
	int			error;

	error = 1;
	ret = NULL;
	ft_strjoin(&ret, buff);
	ft_zero(buff);
	while (error > 0 && ft_return(ret))
	{
		error = read(fd, buff, BUFFER_SIZE);
		ft_strjoin(&ret, buff);
		ft_zero(buff);
	}
	if (error <= 0 && !ft_strlen(ret))
	{
		free(ret);
		ret = NULL;
		return (ret);
	}
	i = 0;
	while (ret[i] && ret[i] != '\n')
		i++;
	if (ret[i] == '\n')
	{
		i++;
		j = 0;
		while (ret[i])
		{
			buff[j] = ret[i];
			ret[i] = '\0';
			i++;
			j++;
		}
	}
	return (ret);
}
