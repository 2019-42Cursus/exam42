#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>

static void
	ft_putchar(char c)
{
	write(1, &c, 1);
}

static int
	ft_strlen(char *str)
{
	int	i = 0;

	while (str[i])
		i++;
	return (i);
}

static void
	print_d(int nb)
{
	if (nb > 9)
		print_d(nb / 10);
	ft_putchar(nb % 10 + '0');
}

int
	get_len(int nb)
{
	int	i = 0;

	while (nb)
	{
		nb /= 10;
		i++;
	}
	return (i);
}

int
	putnbr(unsigned int nbr, char *base)
{
	int				i = 0;

	if (nbr < 16)
	{
		ft_putchar(base[nbr]);
		return (1);
	}
	else
	{
		i += putnbr(nbr / 16, base);
		i += putnbr(nbr % 16, base);
	}
	return (i);
}

int
	ft_printf(char *str, ...)
{
	va_list			ap;
	char			*s;
	int				nb;
	unsigned int	n;
	int				len;
	int				i = -1;
	int				flen = 0;
	char			*base = "0123456789abcdef";

	va_start(ap, str);
	while (str[++i])
	{
		if (str[i] == '%')
		{
			switch (str[++i])
			{
				case '%':
					flen++;
					write(1, "%", 1);
					break;
				case 's':
					s = va_arg(ap, char *);
					if (s == NULL)
						s = "(null)";
					len = ft_strlen(s);
					write(1, s, len);
					flen += len;
					break;
				case 'd':
					nb = va_arg(ap, int);
					if (nb < 0)
					{
						write(1, "-", 1);
						nb = -nb;
						flen++;
					}
					flen += get_len(nb);
					print_d(nb);
					break;
				case 'x':
					n = va_arg(ap, int);
					flen += putnbr(n, base);
					break;
			}
		}
		else
		{
			write(1, &str[i], 1);
			flen++;
		}
	}
	va_end(ap);
	return (flen);
}

int main(void)
{
	char	*str = NULL;
	int		res1 = ft_printf("Hello %s %d %x %x %% %s\n", "World !", -42, -101, 101, str);
	int		res2 = printf("Hello %s %d %x %x %% %s\n", "World !", -42, -101, 101, str);

	printf("res1 : %d\nres2 : %d\n", res1, res2);
	return (0);
}
