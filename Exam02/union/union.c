/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   union.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 15:46:21 by thzeribi          #+#    #+#             */
/*   Updated: 2020/10/20 19:27:25 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int	ft_check(char *str, char c, int index)
{
	int i;

	i = 0;
	while(i < index)
	{
		if (str[i] == c)
			return(0);
			i++;
	}
	return (1);
}

void ft_union(char *s1, char *s2)
{
	int i;
	int len;

	i = 0;
	while (s1[i])
	{
		if (ft_check(s1, s1[i], i))
			write(1, &s1[i], 1);
		i++;
	}
	len = i;
	i = 0;
	while (s2[i])
	{
		if (ft_check(s2, s2[i], i))
		{
			if (ft_check(s1, s2[i], len))
				write(1, &s2[i], 1);
		}
		i++;
	}
}

int	main(int argc, char *argv[])
{
	if (argc == 3)
		ft_union(argv[1], argv[2]);
	write(1, "\n", 1);
	return (0);
}